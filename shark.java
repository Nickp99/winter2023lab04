
public class shark{
	private String name;
	private int speed;
	private int age;
	
	
	//methods
	public void swim(){
		System.out.println(name + " is swiming fast towards its prey");
	}
	public void hunt(){
		System.out.println(name + " caught and ate the fish, YUNM");
	}
	public void endurance(){
		double endurance = helper();
		System.out.println("the endurance of "+ this.name + " is " + endurance);
	}
	private double helper(){
		double endurance;
		if(this.age <=0){
			endurance = 0.1;
		}
		else{
			endurance=this.speed/this.age;
		}
		return endurance;
	}
	
	//get and set
	public int getSpeed(){
		return this.speed;
	}
	public void setSpeed(int newSpeed){
		this.age = newSpeed;
	}
	public int getAge(){
		return this.age;
	}
	public void setAge(int newAge){
		this.age = newAge;
	}
	public String getName(){
		return this.name;
	}
	public void setName(String newName){
		this.name = newName;
	}
	
	//Constructor
	public shark(String name,int speed,int age){
		this.name =name;
		this.speed = speed;
		this.age=age;
	}
	
}